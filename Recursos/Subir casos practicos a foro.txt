=== ASUNTO ===
Mi desarrollo para Caso Práctico "Etiquetas HTML"

=== CUERPO ===
Saludos a tod@/es.

Adjunto el desarrollo que he implementado para el caso práctico "Etiquetas HTML" del Módulo 3.

Código:

https://gitlab.com/alexcarrascob/curso-diseno-web-sence/blob/master/HTML5%20Demo/WebContent/caso-practico-etiquetas.html

¡Que tengan una buena jornada!